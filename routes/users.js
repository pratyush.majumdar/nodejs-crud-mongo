const express = require('express')
const router = express.Router()
const User = require('../models/user')

router.get('/', async(req,res) => {
	try {
		const user = await User.find()
		res.json(user)
	}catch(err) {
		res.send(err)
	}
})

router.get('/:id', async(req,res) => {
	try {
		const user = await User.findById(req.params.id)
		res.json(user)
	}catch(err) {
		res.send(err)
	}
})

router.post('/', async(req,res) => {
	const user = new User({
		id: req.body.id,
		firstname: req.body.firstname,
		lastname: req.body.lastname,
		emailid: req.body.emailid,
		age: req.body.age
	})

	try {
		const userobj = await user.save()
		res.json(userobj)
	}catch(err) {
		res.send(err)
	}
})

router.put('/:id', async(req,res) => {
	try{
		const user = await User.findById(req.params.id)
		user.id = req.body.id
		user.firstname = req.body.firstname
		user.lastname = req.body.lastname
		user.emailid = req.body.emailid
		user.age = req.body.age

		const userobj = await user.save()
		res.json(userobj)
	}catch(err) {
		res.send(err)
	}
})

router.patch('/:id', async(req,res) => {
    try{
        const user = await User.findById(req.params.id)
        user.age = req.body.age
        const userobj = await user.save()
        res.json(userobj)   
    }catch(err){
        res.send(err)
    }
})

router.delete('/:id', async(req,res) => {
	try{
		const user = await User.findById(req.params.id)
		const userobj = await user.remove()
		res.json(userobj)
	}catch(err) {
		res.send(err)
	}
})

module.exports = router
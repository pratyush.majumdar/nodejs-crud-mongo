const express = require('express')
const mongoose = require('mongoose')
const connection_string = 'mongodb://localhost/users'

const app = express()

mongoose.connect(connection_string, {useNewUrlParser: true, useUnifiedTopology: true})
const connection = mongoose.connection
connection.on('error', function() {
	console.error('MongoDB Connection Error:')
})

app.use(express.json())

app.get('/', (req, res) => {
    res.json({"message": "Welcome to NodeJS and MongoDB CRUD application"});
});

const userRouter = require('./routes/users')
app.use('/users', userRouter)

app.listen('8000', function(){
	console.info('Server Started at http://localhost:8000')
})
const supertest = require("supertest");
const should = require("should");

const server = supertest.agent("http://localhost:8000");

describe("NodeJS CRUD Project Unit Test Cases",function() {
	it("should return home page JSON", function(done) {
		server
            .get("/")
            .expect("Content-type", /json/)
            .expect(200)
            .end(function(err,res) {
                res.status.should.equal(200)
                res.body.should.have.property('message').eql('Welcome to NodeJS and MongoDB CRUD application')
                done()
    	})
	})

    it("should create a user", function(done) {
        let user = {"firstname": "John", "lastname" : "Doe", "emailid" : "john@domain.com", "age" : 30}
        server
            .post("/users")
            .send(user)
            .expect("Content-type", /json/)
            .expect(200)
            .end(function(err,res) {
                res.status.should.equal(200)
                res.body.should.have.property('_id')
                res.body.should.have.property('firstname').eql(user.firstname)
                res.body.should.have.property('lastname').eql(user.lastname)
                res.body.should.have.property('emailid').eql(user.emailid)
                res.body.should.have.property('age').eql(user.age)
                done()
        })
    })

    it("should return all users", function(done) {
        server
            .get("/users")
            .expect("Content-type", /json/)
            .expect(200)
            .end(function(err,res) {
                res.status.should.equal(200)
                res.body.should.be.an.Array()
                res.body[0].should.have.property('_id')
                res.body[0].should.have.property('firstname')
                res.body[0].should.have.property('lastname')
                res.body[0].should.have.property('emailid')
                res.body[0].should.have.property('age')
                done()
        })
    })

    it("should return user with id 6114d5f0423b640a75a827f4", function(done) {
        server
            .get("/users/6114d5f0423b640a75a827f4")
            .expect("Content-type", /json/)
            .expect(200)
            .end(function(err,res) {
                res.status.should.equal(200)
                res.body.should.have.property('_id').eql('6114d5f0423b640a75a827f4')
                done()
        })
    })

    it("should update age to 99 for id 6114d5f0423b640a75a827f4", function(done) {
        let age = {"age" : 99}
        server
            .patch("/users/6114d5f0423b640a75a827f4")
            .send(age)
            .expect("Content-type", /json/)
            .expect(200)
            .end(function(err,res) {
                res.status.should.equal(200)
                res.body.should.have.property('age').eql(99)
                done()
        })
    })

    it("should delete user for id 611e413e025161668dcb8acc", function(done) {        
        server
            .delete("/users/611e413e025161668dcb8acc")
            .expect("Content-type", /json/)
            .expect(200)
            .end(function(err,res) {
                res.status.should.equal(200)
                done()
        })
    })
})
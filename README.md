# NodeJS CRUD MongoDB
Building a Restful CRUD (Create, Retrieve, Update, Delete) API with Node.js, Express and MongoDB. We’ll use Mongoose for interacting with the MongoDB instance.

Express is a minimal and flexible Node.js web application framework. It supports handling for different HTTP verbs (GET, POST, PATCH, DELETE etc.). It can also handle requests at different URL paths ("routes"), serve static files, or use templates to dynamically create the response.

Mongoose is an ODM (Object Document Mapping) tool for Node.js and MongoDB. It helps you convert the objects in your code to documents in the database and vice versa.

## Commands to install Project
```javascript
npm init
npm install express
npm install mongodb
npm install mongoose
sudo npm install -g nodemon --save-dev

sudo npm install -g mocha --save-dev
npm install should --save-dev
npm install supertest --save-dev
```

## Run Project in development mode
```javascript
nodemon run start
```

## Run Project in production mode
```javascript
npm start
```

## Run Test cases
```javascript
npm test
```

## Rest APIs
- POST request to http://localhost:8000/users to add a user
- GET request to http://localhost:8000/users to get all users
- GET request to http://localhost:8000/users/<mongodb_id> to get specific user
- PATCH request to http://localhost:8000/users/<mongodb_id> to change the age of the user
- PUT request to http://localhost:8000/users/<mongodb_id> to edit the user
- DELETE request to http://localhost:8000/users/<mongodb_id> to delete the user

```json
{
	"firstname" : "John",
	"lastname" : "Doe",
	"emailid" : "john@domain.com",
	"age" : 35
}
```
